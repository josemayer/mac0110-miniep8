function troca(v, i, j)
    aux = v[i]
    v[i] = v[j]
    v[j] = aux
end
function compareByValue(x, y)
    p = length(x) - 1
    m = length(y) - 1
    val1 = SubString(x, 1, p)
    val2 = SubString(y, 1, m)
    ord = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"]
    tam = length(ord)
    a = 0
    b = 0
    for i in 1:tam
        if ord[i] == val1
            a = i
        end
        if ord[i] == val2
            b = i
        end
    end
    if a < b
        return true
    else
        return false
    end
end
function compareByValueAndSuit(x, y)
    value1 = SubString(x, length(x))
    value2 = SubString(y, length(y))
    ordsui = ["♦", "♠", "♥", "♣"]
    tam = length(ordsui)
    c = 0
    d = 0
    for j in 1:tam
        if ordsui[j] == value1
            c = j
        end
        if ordsui[j] == value2
            d = j
        end
    end
    if c < d
        return true
    elseif c > d
        return false
    else
        compareByValue(x, y)
    end
end
function insercao(v)
    tam = length(v)
    for i in 2:tam
        j = i
        while j > 1
            if compareByValueAndSuit(v[j], v[j - 1])
                troca(v, j, j - 1)
            else
                break
            end
            j = j - 1
        end
    end
    return v
end

using Test

function teste_parte1()
    @test compareByValue("2♠", "A♠") == true
    @test compareByValue("K♥", "10♥") == false
    @test compareByValue("10♠", "10♥") == false
    @test compareByValue("J♥", "Q♥") == true
    @test compareByValue("K♠", "A♦") == true
    @test insercao(["10♥", "10♦", "K♠", "A♠", "J♠", "A♠"]) == ["10♥", "10♦", "J♠", "K♠", "A♠", "A♠"]
    @test insercao(["K♣", "A♠", "7♠"]) == ["7♠", "K♣", "A♠"]
    println("Conclusão do teste da parte 1")
end
function teste_parte2()
    @test compareByValueAndSuit("2♠", "A♠") == true
    @test compareByValueAndSuit("K♥", "10♥") == false
    @test compareByValueAndSuit("10♠", "10♥") == true
    @test compareByValueAndSuit("J♥", "Q♥") == true
    @test compareByValueAndSuit("K♠", "A♦") == false
    @test compareByValueAndSuit("A♠", "2♥") == true
    @test compareByValueAndSuit("A♠", "A♠") == false
    @test compareByValueAndSuit("2♦", "A♣") == true
    @test insercao(["10♥", "10♦", "K♠", "A♠", "J♠", "A♠"]) == ["10♦", "J♠", "K♠", "A♠", "A♠", "10♥"]
    @test insercao(["9♣", "8♠", "7♥", "6♦"]) == ["6♦", "8♠", "7♥", "9♣"]
    @test insercao(["3♦", "Q♠", "J♣"]) == ["3♦", "Q♠", "J♣"]
    println("Conclusão do teste da parte 2")
end
